/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main(){

	int tabla;
	int tope;
	int A;
	int resultado;

	cout<<"Digite la tabla que quiera ver: ";
	cin>>tabla;
	cout<<"¿Hasta donde la desea visualizar?: ";
	cin>>tope;

	A = 1;

	while ( A <= tope){
	
		resultado = tabla * A;
		cout<< tabla << " x "<< A << " = " << resultado << "\n";
		A++;
	
	}

}
