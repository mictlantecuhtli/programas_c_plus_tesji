/*
 * Autor: Fátima Azucena Martínez Cadena
 * Fecha: 12_12_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main() {

	//Declaración de variables
	int a1[5];
        int a2[5];
        int result[5];
        int i;
        int y;
        int a;
        int c;
        int z;
        int b = 4;

	cout<<"\nArreglo 1 \n";
	for ( i = 0; i < 5; i++ ) {
		cout<<"Digite el número en la posición "<<i<<": ";
		cin>>a1[i];
	}
	cout<<"\n";
	for ( y = 0; y < 5; y++ ) {
		cout<<a1[y]<<" ";
	}
	cout<<"\n\n";
	for ( a = 0; a < 5; a++ ) {
                cout<<"Digite el número en la posición "<<a<<": ";
                cin>>a2[a];
        }
	cout<<"\n";
        for ( c = 0; c < 5; c++ ) {
                cout<<a2[c]<<" ";
        }
	cout<<"\n\nResultado\n";
	for ( z = 0; z < 5; z++ ) {
		result [z] = a1[z]*a2[b];
		b--;
		cout<<result[z]<<" ";
	}
	cout<<"\n";
}
