/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main(){
	
	int n;
	int A;
	string nombre;

	cout<<"Ingrese su nombre completo: ";
	cin>>nombre;
	cout<<"Ingrese cuantas veces desea que se repita su nombre: ";
	cin>>n;

	A = 1;

	while ( A <= n){
	
		cout<<"Tu nombre completo es: "<<nombre;
		A++;
		cout<<"\n";

	}

}
