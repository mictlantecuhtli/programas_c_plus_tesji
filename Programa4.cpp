/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

# include <iostream>
using namespace std;

int main(){

	int fact;
	int num;
	int b;
	int A;

	cout<<"Digite un numero el cual desea ver el factorial: ";
	cin>>num;

	A = 1;
	fact = 1;

	while ( A <= num){
	
		b = A * fact;
		cout<< fact << " x " << A << " = "<< b << "\n";
		fact = fact * A;
		A++;
	
	}

}
