#include <iostream>
 
using namespace std;
 
int main()
{
    int par = 0, impar = 0;
    int numeros[8];
    int pares[8];
    int impares[8];
 
    cout << "\nIntroducir datos:" << endl;
    for (int i = 0; i < 8; i++)
    {
        cout << "(" << i + 1 << "/8): ";
        cin >> numeros[i];
 
        if (numeros[i] % 2 == 0)
            pares[par++] = numeros[i];
        else
            impares[impar++] = numeros[i];
    }
 
    cout << "\nNumeros Pares: ";
    for (int i = 0; i < par; i++)
        cout << pares[i] << " ";
 
    cout << "\nNumeros Impares: ";
    for (int i = 0; i < impar; i++)
        cout << impares[i] << " ";
 
    cout << endl;
 
    return 0;
}
