/*
* Fecha: 04_10_22
* Autor: Fátima Azucena MC
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main(){

	int anyos;
	int dias;
	int diasExtras;

	cout<<"Si no llevas un año trabajando, digita tus meses laborados"<<"\n";
	cout<<"¿Cuántos años lleva trabajando?: ";
	cin>>anyos;

	if ( anyos < 1 ) {
	
		cout<<"Lo sentimos, no cumples un año laborando, no tienes vacaciones";

	}
	
	else if ( anyos >= 1 && anyos <= 5 ) {
	
		cout<<"Cuentas con 5 días de vacaciones";
	
	}

	else if ( anyos >= 5 && anyos <= 10 ) {
	
		cout<<"Cuentas con 8 dias de vacaciones";

	}

	else if ( anyos > 10 ) {
	
		dias = anyos - 10;
		diasExtras = 8 + dias;
		cout<<"Cuentas con "<< diasExtras <<" días de vacaciones";

	}



}

