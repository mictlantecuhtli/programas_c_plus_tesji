/*
 * Nombre: Fátima Azucena MC
 * Fecha: 17_08_22
 * Correo electronico: fatimaazucenamartinez274@gmail.com
*/

# include <iostream>
using namespace std;

int main(){

	int n;
	int suma;
	int A;

	cout<<"Digite un numero: ";
	cin>>n;

	A = 1;

	while ( A <= n){
	
		suma = suma + A;
		A++;

	}

	cout<<"El resultado de la suma es: " << suma<<"\n";
}
