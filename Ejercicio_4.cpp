/*
 * Autor: Fatima Azucena MC
 * Fecha: 10_11_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main(){

	int opcion;
        int cantidad;
        int total;

        cout<<"Tallas disponibles:\n1.-Talla chica\n2.-Talla mediana\n3.-Talla grande\nDigite su opcion: ";
        cin>>opcion;

        switch (opcion) {
                case 1:
                        cout<<"\nA elegido talla chica (Talla 7)\n¿Cuántos vestidos compró?: ";
                        cin>>cantidad;
                        total = cantidad * 300;
                break;
                case 2:
                        cout<<"\nA elegido talla mediana (Talla 12)\n¿Cuántos vestidos compró?: ";
                        cin>>cantidad;
                        total = cantidad * 400;
                break;
                case 3:
                        cout<<"\nA elegido talla grande (Talla 16)\n¿Cuántos vestidos compró?: ";
                        cin>>cantidad;
                        total = cantidad * 500;
                break;
                default:
                        cout<<"\nOpción inválida";
        }

        cout<<"\nEl total a pagar es: "<<total<<"\n";
 

}
