/*
 * Autor: Fatima Azucena MC
 * Fecha: 09_09_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;

int main(){

	int numero, doble, tercera, mitad;

	numero = 30;
	doble = numero * 2;
	tercera = doble / 3;
	mitad = tercera / 2;

	cout<<"La mitad de la tercera parte de 30 es: "<<mitad;

}
